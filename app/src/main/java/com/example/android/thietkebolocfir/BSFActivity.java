package com.example.android.thietkebolocfir;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class BSFActivity extends AppCompatActivity {

    EditText wsEditText1, wsEditText2, wpEditText1, wpEditText2, asEditText, w1EditText, w2EditText, w3EditText;
    double as, ws1, ws2, wp1, wp2, w1, w2, w3; //wp1 < ws1 < ws2 < wp2

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bsf);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        wsEditText1 = (EditText) findViewById(R.id.ws1_edit_text);
        wsEditText2 = (EditText) findViewById(R.id.ws2_edit_text);
        wpEditText1 = (EditText) findViewById(R.id.wp1_edit_text);
        wpEditText2 = (EditText) findViewById(R.id.wp2_edit_text);
        asEditText = (EditText) findViewById(R.id.as_edit_text);
        w1EditText = (EditText) findViewById(R.id.w1_edit_text);
        w2EditText = (EditText) findViewById(R.id.w2_edit_text);
        w3EditText = (EditText) findViewById(R.id.w3_edit_text);
    }

    public void designButtonClick(final View view) {

        try {
            ws1 = Double.parseDouble(wsEditText1.getText().toString()) * Math.PI;
        } catch (NumberFormatException e) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Vui lòng chỉ nhập số");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wsEditText1.setText("");
            wsEditText1.requestFocus();
            return;
        }

        try {
            ws2 = Double.parseDouble(wsEditText2.getText().toString()) * Math.PI;
        } catch (NumberFormatException e) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Vui lòng chỉ nhập số");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wsEditText2.setText("");
            wsEditText2.requestFocus();
            return;
        }

        try {
            wp1 = Double.parseDouble(wpEditText1.getText().toString()) * Math.PI;
        } catch (NumberFormatException e) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Vui lòng chỉ nhập số");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wpEditText1.setText("");
            wpEditText1.requestFocus();
            return;
        }

        try {
            wp2 = Double.parseDouble(wpEditText2.getText().toString()) * Math.PI;
        } catch (NumberFormatException e) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Vui lòng chỉ nhập số");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wpEditText2.setText("");
            wpEditText2.requestFocus();
            return;
        }

        try {
            as = Double.parseDouble(asEditText.getText().toString());
        } catch (NumberFormatException e) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Vui lòng chỉ nhập số");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            asEditText.setText("");
            asEditText.requestFocus();
            return;
        }

        if (!((ws1 > 0) && (ws1 < Math.PI))) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("0 < Ws < PI. Vui lòng nhập lại");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wsEditText1.requestFocus();
            return;
        }

        if (!((wp1 > 0) && (wp1 < Math.PI))) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("0 < Wp < PI. Vui lòng nhập lại");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wpEditText1.requestFocus();
            return;
        }

        if (!((ws2 > 0) && (ws2 < Math.PI))) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("0 < Ws < PI. Vui lòng nhập lại");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wsEditText2.requestFocus();
            return;
        }

        if (!((wp2 > 0) && (wp2 < Math.PI))) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("0 < Wp < PI. Vui lòng nhập lại");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wpEditText2.requestFocus();
            return;
        }

        if (!((wp1 < ws1) && (ws1 < ws2) && (ws2 < wp2))) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Bộ lọc thông dải Wp1 < Ws1 < Ws2 < Wp2.\nVui lòng nhập lại");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    wsEditText1.requestFocus();
                }
            });
            dialog.show();
            return;
        }

        w1 = Double.parseDouble(w1EditText.getText().toString()) * Math.PI;
        w2 = Double.parseDouble((w2EditText.getText().toString())) * Math.PI;
        w3 = Double.parseDouble(w3EditText.getText().toString()) * Math.PI;

        Bundle bundle = new Bundle();
        bundle.putDouble("ws1", ws1);
        bundle.putDouble("wp1", wp1);
        bundle.putDouble("ws2", ws2);
        bundle.putDouble("wp2", wp2);
        bundle.putDouble("as", as);
        bundle.putDouble("w1", w1);
        bundle.putDouble("w2", w2);
        bundle.putDouble("w3", w3);

        Intent intent = new Intent(this, BSFGraph.class);
        intent.putExtra("data", bundle);
        startActivity(intent);
    }

    public void resetButtonClick(View view) {
        wsEditText1.setText("");
        wsEditText2.setText("");
        wpEditText1.setText("");
        wpEditText2.setText("");
        asEditText.setText("");
    }

}
