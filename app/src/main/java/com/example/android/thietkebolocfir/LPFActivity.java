package com.example.android.thietkebolocfir;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

public class LPFActivity extends AppCompatActivity {

    EditText wsEditText, wpEditText, asEditText, w1EditText, w2EditText;
    LinearLayout lpfLayout;

    double ws, wp, as, w1, w2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lpf);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lpfLayout = (LinearLayout) findViewById(R.id.lpfGrap_content);
        wsEditText = (EditText) findViewById(R.id.ws_edit_text);
        wpEditText = (EditText) findViewById(R.id.wp_edit_text);
        asEditText = (EditText) findViewById(R.id.as_edit_text);
        w1EditText = (EditText) findViewById(R.id.w1_edit_text);
        w2EditText = (EditText) findViewById(R.id.w2_edit_text);

    }

    public void OKButtonClick(View view) {

        try {
            ws = Double.parseDouble(wsEditText.getText().toString()) * Math.PI;

        } catch (NumberFormatException e) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Vui lòng chỉ nhập số");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wsEditText.setText("");
            wsEditText.requestFocus();
            return;
        }

        try {
            wp = Double.parseDouble(wpEditText.getText().toString()) * Math.PI;

        } catch (NumberFormatException e) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Vui lòng chỉ nhập số");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wpEditText.setText("");
            wpEditText.requestFocus();
            return;
        }

        try {
            as = Double.parseDouble(asEditText.getText().toString());

        } catch (NumberFormatException e) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Vui lòng chỉ nhập số");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            asEditText.setText("");
            asEditText.requestFocus();
            return;
        }

        if (!((ws > 0) && (ws < Math.PI))){
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("0 < Ws < PI. Vui lòng nhập lại");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wsEditText.requestFocus();
            return;
        }

        if (!((wp > 0) && (wp < Math.PI))){
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("0 < Wp < PI. Vui lòng nhập lại");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wpEditText.requestFocus();
            return;
        }

        if (wp >= ws) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Lỗi dữ liệu");
            dialog.setMessage("Chú ý: Đây là bộ lọc thông thấp Ws > Wp");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();
            wsEditText.requestFocus();
            wpEditText.requestFocus();
            return;
        }

        w1 = Double.parseDouble(w1EditText.getText().toString()) * Math.PI;
        w2 = Double.parseDouble(w2EditText.getText().toString()) * Math.PI;

        Bundle bundle = new Bundle();

        bundle.putDouble("As", as);
        bundle.putDouble("Ws", ws);
        bundle.putDouble("Wp", wp);
        bundle.putDouble("W1", w1);
        bundle.putDouble("W2", w2);

        Intent intent = new Intent(this, LPFGraph.class);
        intent.putExtra("Data", bundle);
        startActivity(intent);
    }

    public void resetButtonClick(View view) {
        wsEditText.setText("");
        wpEditText.setText("");
        asEditText.setText("");
    }

}
