package com.example.android.thietkebolocfir;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.CombinedXYChart;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.ScatterChart;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

/**
 * Created by solei on 08/12/2015.
 */
public class Graph {

    public View getCombineXYChartView(Context context, TimeSeries series, TimeSeries series2, String XTitle, String YTitle) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(series);
        dataset.addSeries(series2);

        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
        XYSeriesRenderer renderer = new XYSeriesRenderer();
        mRenderer.addSeriesRenderer(renderer);
        mRenderer.setXLabelsColor(Color.RED);
        mRenderer.setYLabelsColor(DefaultRenderer.NO_COLOR, Color.RED);
        mRenderer.setXAxisColor(Color.BLACK);
        mRenderer.setYAxisColor(Color.BLACK);
        mRenderer.setLabelsColor(Color.BLACK);
        mRenderer.setApplyBackgroundColor(true);
        mRenderer.setBackgroundColor(Color.WHITE);
        mRenderer.setMarginsColor(Color.WHITE);
        mRenderer.setXTitle(XTitle);
        mRenderer.setYTitle(YTitle);

        XYSeriesRenderer renderer2 = new XYSeriesRenderer();
        renderer2.setPointStyle(PointStyle.CIRCLE);
        renderer2.setFillPoints(true);
        mRenderer.addSeriesRenderer(renderer2);
        mRenderer.setPointSize(3.5f);

        CombinedXYChart.XYCombinedChartDef[] types = {new CombinedXYChart.XYCombinedChartDef(LineChart.TYPE, 0),
                new CombinedXYChart.XYCombinedChartDef(ScatterChart.TYPE, 1)};

        GraphicalView impulseReponseView = ChartFactory.getCombinedXYChartView(context, dataset, mRenderer, types);

        return impulseReponseView;
    }

    public View getLineChartView(Context context, TimeSeries series, String XTitle, String YTitle) {
        XYMultipleSeriesDataset dataset1 = new XYMultipleSeriesDataset();
        dataset1.addSeries(series);

        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setLineWidth(2);
        mRenderer.addSeriesRenderer(renderer);
        mRenderer.setApplyBackgroundColor(true);
        mRenderer.setBackgroundColor(Color.WHITE);
        mRenderer.setMarginsColor(Color.WHITE);
        mRenderer.setXLabelsColor(Color.RED);
        mRenderer.setYLabelsColor(DefaultRenderer.NO_COLOR, Color.RED);
        mRenderer.setXAxisColor(Color.BLACK);
        mRenderer.setYAxisColor(Color.BLACK);
        mRenderer.setLabelsColor(Color.BLACK);
        mRenderer.setXTitle(XTitle);
        mRenderer.setYTitle(YTitle);

        View magnitudeReponseView = ChartFactory.getLineChartView(context, dataset1, mRenderer);
        return magnitudeReponseView;
    }
}
