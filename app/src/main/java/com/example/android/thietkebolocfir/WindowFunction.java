package com.example.android.thietkebolocfir;

/**
 * Created by soleil on 26/11/2015.
 */
public class WindowFunction {

    //Cửa sổ chữ nhật
    public int Rect(int n, int N) {
        if ((n >= 0) && (n <= N)) return 1;
        else return 0;
    }

    //Cửa sổ Tam Giác
    public double Triangle(int n, int N) {
        if ((n >= 0) && (n <= N/2)) return 2 * n / N;
        else if (n <= N) return 2 - 2 * n / N;
        else return 0;
    }

    //Hanning
    public double Hanning(int n, int N) {
        if ((n >= 0) && (n <= N)) return (0.5 - 0.5 * Math.cos(2*Math.PI * n / N));
        else return 0;
    }

    //Hamming
    public double Hamming(int n, int N) {
        if ((n >= 0) && (n <= N)) return (0.54 - 0.46 * Math.cos(2*Math.PI * n / N));
        else return 0;
    }

    //BlackMan
    public double BlackMan(int n, int N) {
        if ((n >= 0) && (n <= N)) return (0.42 - 0.5 * Math.cos(2*Math.PI * n / N) + 0.08 * Math.cos(4 * Math.PI * n / N));
        else return 0;
    }

}
