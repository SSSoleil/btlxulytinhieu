package com.example.android.thietkebolocfir;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_abouts) {
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("About Me");
            dialog.setMessage("Design by NGUYEN Van Viet - KSCLC HTHT&TT K57\nEmail: vietnv1304@gmail.com\nThanks for using!");
            dialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //thông thấp
    public void lpfButtonClick(View view) {
        Intent intent = new Intent(this, LPFActivity.class);
        startActivity(intent);

    }

    //thông cao
    public void hpfButtonClick(View view) {
        Intent intent = new Intent(this, HPFActivity.class);
        startActivity(intent);
    }

    //thông dải
    public void bpfButtonClick(View view) {
        Intent intent = new Intent(this, BPFActivity.class);
        startActivity(intent);
    }

    //chắn dải
    public void bsfButtonClick(View view) {
        Intent intent = new Intent(this, BSFActivity.class);
        startActivity(intent);
    }
}
