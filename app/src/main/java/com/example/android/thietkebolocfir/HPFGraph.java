package com.example.android.thietkebolocfir;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TabHost;

import org.achartengine.model.TimeSeries;

public class HPFGraph extends AppCompatActivity {

    LinearLayout hpfGaphLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hpfgraph);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadTabs();

        hpfGaphLayout = (LinearLayout) findViewById(R.id.tab1);
        LinearLayout hpfGraphLayoutTest = (LinearLayout) findViewById(R.id.tab2);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        double ws = bundle.getDouble("Ws");
        double wp = bundle.getDouble("Wp");
        double as = bundle.getDouble("As");

        WindowFunction windowFunction = new WindowFunction();

        int checkAs;
        int N, alpha;

        double wc = (ws + wp) / 2;
        double wn = 0;

        double deltaW = (wp - ws);

        Log.e("ws", ws + "");
        Log.e("as", as + "");


        if (as <= 21) {
            checkAs = 1;
            N = (int) (1.8 * Math.PI / deltaW);  //Chu nhat
        } else if (as <= 25) {
            checkAs = 2;
            N = (int) (6.1 * Math.PI / deltaW); //tam giac
        } else if (as <= 44) {
            checkAs = 3;
            N = (int) (6.2 * Math.PI / deltaW); //Hanning
        } else if (as <= 55) {
            checkAs = 4;
            N = (int) (6.6 * Math.PI / deltaW); //Hamming
        } else {
            checkAs = 5;
            N = (int) (11 * Math.PI / deltaW); //blackMan
        }


        Log.e("N", N + "");
        alpha = N / 2;

        double[] h = new double[N + 1];
        double[] n = new double[N + 1];
        for (int i = 0; i < n.length; i++) {
            n[i] = i;
            switch (checkAs) {
                case 1:
                    wn = windowFunction.Rect(i, N);
                    break;
                case 2:
                    wn = windowFunction.Triangle(i, N);
                    break;
                case 3:
                    wn = windowFunction.Hanning(i, N);
                    break;
                case 4:
                    wn = windowFunction.Hamming(i, N);
                    break;
                case 5:
                    wn = windowFunction.BlackMan(i, N);
            }
            if (i == N / 2) {
                h[i] = (1 - wc / Math.PI) * wn;
            } else
                h[i] = (deltaN((int) n[i] - alpha) - (Math.sin(wc * (n[i] - alpha)) / (Math.PI * (n[i] - alpha)))) * wn;
        }

        double[] omega = new double[5* N + 1]; //omega
        double[] modulH = new double[5* N + 1]; //dap ung bien do
        double real, im;
        for (int i = 0; i < omega.length; i++) {
            real = 0;
            im = 0;
            omega[i] = i * Math.PI / (5 * N);
            for (int j = 0; j < h.length; j++) {
                real += h[j] * Math.cos(omega[i] * j);
                im += h[j] * Math.sin(omega[i] * j);
            }
            modulH[i] = Math.sqrt(real * real + im * im);
        }

        double w1 = bundle.getDouble("W1");
        double w2 = bundle.getDouble("W2");
        //Tính các giá trị của tín hiệu test x(n) = sin(ω1 * n) + sin(ω2 * n)
        double x[] = new double[5 * N + 1];
//        double y[] = new double[N + 1];

        double[] modulXw = new double[5 * N + 1];

        for (int i = 0; i < x.length; i++) {
            x[i] = Math.sin(w1 * i) + Math.sin(w2 * i);
        }
        for (int i = 0; i < x.length; i++) {
            real = 0;
            im = 0;
            omega[i] = i * Math.PI / (5*N);
            for (int j = 0; j < x.length; j++) {
                real += x[j] * Math.cos(omega[i] * j);
                im += x[j] * Math.sin(omega[i] * j);
            }
            modulXw[i] = Math.sqrt(real * real + im * im);
        }

        double[] modulYw = new double[5*N + 1];
        //tính y(n)
//        for (int i = 0; i < n.length; i++) {
//            for (int k = 0; k < i; k++) {
//                y[i] += h[k] * x[i - k];
//            }
//        }
//        for (int i = 0; i < x.length; i++) {
//            real = 0;
//            im = 0;
//            omega[i] = i * Math.PI / N;
//            for (int j = 0; j < h.length; j++) {
//                real += y[j] * Math.cos(omega[i] * j);
//                im += y[j] * Math.sin(omega[i] * j);
//            }
//            modulYw[i] = Math.sqrt(real * real + im * im);
//        }

        for (int i = 0; i < modulYw.length; i++) {
            modulYw[i] = modulXw[i] * modulH[i];
        }

        TimeSeries seriesYw = new TimeSeries("Y(ω)");
        for (int i = 0; i < modulYw.length; i++) {
            seriesYw.add(omega[i], modulYw[i]);
        }

        TimeSeries seriesXw = new TimeSeries("X(ω)");
        for (int i = 0; i < omega.length; i++) {
            seriesXw.add(omega[i], modulXw[i]);
        }

        TimeSeries series1 = new TimeSeries("Đáp ứng biên độ");
        for (int i = 0; i < omega.length; i++) {
            series1.add(omega[i], modulH[i]);
        }

        TimeSeries series = new TimeSeries("Đáp ứng xung");
        for (int i = 0; i < n.length; i++) {
            series.add(n[i], h[i]);
            series.add(n[i] - 0.0001, 0);
            series.add(n[i] + 0.0001, 0);
        }

        TimeSeries series2 = new TimeSeries("");
        for (int i = 0; i < n.length; i++) {
            series2.add(n[i], h[i]);
        }

        Graph graph = new Graph();
        View impulseReponseView = graph.getCombineXYChartView(this, series, series2, "n", "h(n)");

        View magnitudeReponseView = graph.getLineChartView(this, series1, "ω", "H(ω");

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.VERTICAL);

        impulseReponseView.setLayoutParams(layoutParams);
        magnitudeReponseView.setLayoutParams(layoutParams);
        hpfGaphLayout.addView(impulseReponseView);
        hpfGaphLayout.addView(magnitudeReponseView);

        View Xw = graph.getLineChartView(this, seriesXw, "ω", "Y(ω)");
        View Yw = graph.getLineChartView(this, seriesYw, "ω", "Y(ω)");
        Xw.setLayoutParams(layoutParams);
        Yw.setLayoutParams(layoutParams);
        hpfGraphLayoutTest.addView(Xw);
        hpfGraphLayoutTest.addView(Yw);
    }

    public int deltaN(int n) {
        if (n == 0) return 1;
        else return 0;
    }

    public void loadTabs() {
        final TabHost tab = (TabHost) findViewById
                (android.R.id.tabhost);

        tab.setup();
        TabHost.TabSpec spec;

        spec = tab.newTabSpec("t1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Đáp ứng xung / Biên độ");
        tab.addTab(spec);

        spec = tab.newTabSpec("t2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Tín hiệu Test");
        tab.addTab(spec);
        tab.setCurrentTab(0);
    }
}
